#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-27 12:48-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.6.0\n"

#: news.html%2Bhtml[lang]:4-1
msgid "en"
msgstr ""

#: news.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr ""

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: news.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr ""

#: news.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr ""

#: news.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr ""

#: news.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr ""

#: news.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr ""

#: news.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr ""

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: news.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr ""

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: news.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news.html\""
">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" href=\"../"
"es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" hreflang=\"fa"
"\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-item\" lang=\"fr\" "
"hreflang=\"fr\" href=\"../fr/news.html\">Français</a> <a class=\"navbar-"
"item\" lang=\"my\" hreflang=\"my\" href=\"../my/news.html\">မြန်မာစာ</a> <a "
"class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/news.html\""
">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" href=\"../"
"tr/news.html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk"
"\" href=\"../uk/news.html\">Українська</a> <a class=\"navbar-item\" lang=\"ur"
"\" hreflang=\"ur\" href=\"../ur/news.html\">اردو</a>"
msgstr ""

#: news.html%2Bhtml.body.main.title:135-1
msgctxt "news.html+html.body.main.title:135-1"
msgid "CENO Browser | News and Media"
msgstr ""

#: news.html%2Bhtml.body.main.title:136-1
msgctxt "news.html+html.body.main.title:136-1"
msgid "CENO Browser | News and Media"
msgstr ""

#: news.html%2Bhtml.body.main.div.h1:138-5
msgid "News and Media"
msgstr ""

#: news.html%2Bhtml.body.main.div.p:139-2
msgid ""
"Read the latest updates from the CENO team, check out past coverage, and "
"find resources for journalists and researchers."
msgstr ""

#: news.html%2Bhtml.body.main.div.section.h2:143-2
msgid "Our News"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:151-17
msgid "CENO proves itself in Iran during Internet shutdowns"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:153-17
msgid "December 15, 2022"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:164-17
msgid "eQualitie launches CENO, world's first decentralized p2p mobile browser"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:166-3
msgid "May 10th, 2022"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.h2:177-1
msgid "Recent Coverage"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:185-17
msgid "WhatsApp Launches a Tool to Fight Internet Censorship"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:187-17
msgid "January 5th, 2023"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:198-17
msgid ""
"A new media project is working to sneak blocked media into Russia and wants "
"to become a hub for press freedom in totalitarian regimes"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:200-17
msgid "September 1st, 2022"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:211-17
msgid ""
"The CENO browser is gaining popularity on the Internet - everything opens "
"with it without a VPN"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:213-17
msgid "August 12th, 2022"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.p:219-9
msgid "More Coverage >>"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.h2:225-1
msgid "Events & Presentations"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:234-17
msgid "CENO: Avoid Internet Shutdowns with Cooperative Web Browsing (Arabic)"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.p:235-17
msgid "November 26th, 2021"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:246-17
msgid "CENO: Route Around Censorship with P2P-powered Web Browsing"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.p:247-17
msgid "November 4th, 2021"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:258-17
msgid ""
"Ouinet, a cooperative P2P web cache that helps overcome network interference"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.p:259-17
msgid "August 2nd, 2018"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.h2:272-1
msgid "Resources"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:279-17
msgid "Fact Sheet"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:280-17
msgid "Learn"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:291-17
msgid "Case Studies"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:292-17
msgid "Read"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:303-17
msgid "Brand & Style Guide"
msgstr ""

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:304-3
msgid "Explore"
msgstr ""

#: news.html%2Bhtml.body.main.div.p:315-2
msgid ""
"For press and media inquiries, please contact Jenny Ryan at: press [at] "
"equalitie [dot] org"
msgstr ""

#: news.html%2Bhtml.body.footer.div.p:332-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""

#.  TRANSLATORS: Please do not translate this. 
#: news.html%2Bhtml.body.footer:334-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document.write(\""
"-\"+new Date().getFullYear());</script>"
msgstr ""
