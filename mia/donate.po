msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-21 14:16-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: mia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.6.0\n"

#: donate.html%2Bhtml[lang]:4-1
msgid "en"
msgstr ""

#: donate.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr ""

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: donate.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr ""

#: donate.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr ""

#: donate.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr ""

#: donate.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr ""

#: donate.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr ""

#: donate.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr ""

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: donate.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr ""

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: donate.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/donate."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/donate.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/donate.html\">فارسی</a> <a class=\"navbar-"
"item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/donate.html\">Français</a> "
"<a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/donate."
"html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
"href=\"../ru/donate.html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" "
"hreflang=\"tr\" href=\"../tr/donate.html\">Türkçe</a> <a class=\"navbar-"
"item\" lang=\"uk\" hreflang=\"uk\" href=\"../uk/donate.html\">Українська</a> "
"<a class=\"navbar-item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/donate."
"html\">اردو</a>"
msgstr ""

#: donate.html%2Bhtml.body.main.title:135-1
msgid "CENO Browser | Download"
msgstr ""

#: donate.html%2Bhtml.body.main.title:136-1
msgid "CENO Browser | Donate"
msgstr ""

#: donate.html%2Bhtml.body.main.div.h1:138-6
msgid "Donate to Ceno Browser"
msgstr ""

#: donate.html%2Bhtml.body.main.div.p:139-3
msgid ""
"Ceno Browser is completely free and open source software funded solely "
"through grants and donations. Below are some of the ways you can support its "
"continued development through financial contributions. For other ways of "
"getting involved and contributing, please visit <a href=\"help.html\">the "
"Community page</a>!"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:148-18
msgid "Make a Donation"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:149-11
msgid "Make a one-time donation using our Stripe payment portal."
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:150-18
msgid "Donate via Stripe"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:161-17
msgid "Become a Sponsor"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:162-3
msgid "We are currently exploring fundraising platforms and communities:"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:163-17
msgid "Sponsor on Github"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:174-20
msgid "Donate Bitcoin"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:175-3
msgid "Our wallet address is:"
msgstr ""

#: donate.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:176-4
msgid "bc1q2r70hf0jks43jyykpua6uqv907zegplgfacauc"
msgstr ""

#: donate.html%2Bhtml.body.footer.div.p:200-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""

#.  TRANSLATORS: Please do not translate this. 
#: donate.html%2Bhtml.body.footer:202-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"
msgstr ""
